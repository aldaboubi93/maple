package com.abtotest.voiptest;

import org.abtollc.sdk.AbtoApplication;
import org.abtollc.sdk.AbtoPhone;
import org.abtollc.sdk.OnRegistrationListener;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.os.Vibrator;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnRegistrationListener, View.OnClickListener,
        View.OnLongClickListener {

    private String remoteContact;
    private String domain;
    private AbtoPhone abtoPhone;
    private ProgressDialog dialog;

    private Button audioCallButton;
    //    private Button videoCallButton;
//    private Button sendImButton;
    private EditText callUri;
    private TextView accLabel;
    int accExpire;


    private EditText mPhoneNumberField;
    private Button mOneButton;
    private Button mTwoButton;
    private Button mThreeButton;
    private Button mFourButton;
    private Button mFiveButton;
    private Button mSixButton;
    private Button mSevenButton;
    private Button mEightButton;
    private Button mNineButton;
    private Button mZeroButton;
    private Button mStarButton;
    private Button mPoundButton;
    private Button mDeleteButton;

    private static final int DURATION = 50; // Vibrate duration

    private Vibrator mVibrator; // Vibration (haptic feedback) for dialer key presses.


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Get AbtoPhone instance
        abtoPhone = ((AbtoApplication) getApplication()).getAbtoPhone();

        // Set registration event
        abtoPhone.setRegistrationStateListener(this);

        audioCallButton = (Button) findViewById(R.id.start_audio_call);
//        videoCallButton = (Button) findViewById(R.id.start_video_call);
//        sendImButton = (Button) findViewById(R.id.send_im);
        callUri = (EditText) findViewById(R.id.sip_number);

        int accId = (int) abtoPhone.getCurrentAccountId();
        accExpire = abtoPhone.getConfig().getAccountExpire(accId);


        accLabel = (TextView) findViewById(R.id.account_label);
        String contact = abtoPhone.getConfig().getAccount(accId).acc_id;
        contact = contact.replace("<", "");
        contact = contact.replace(">", "");

        if (accExpire == 0) {
            accLabel.setText("Local contact: " + contact + ":" + abtoPhone.getConfig().getSipPort());
            callUri.setHint("number@domain:port");
            domain = "";
        } else {
            accLabel.setText("Registered as : " + contact);
            domain = abtoPhone.getConfig().getAccountDomain(accId);
        }


        audioCallButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                startCall(false);
            }
        });

//        videoCallButton.setOnClickListener(new View.OnClickListener() {
//
//            public void onClick(View v) {
//                startCall(true);
//            }
//        });
//
//        sendImButton.setOnClickListener(new View.OnClickListener() {
//
//            public void onClick(View v) {
//
//                try{
//                    String sipNumber = callUri.getText().toString();
//                    abtoPhone.sendTextMessage(abtoPhone.getCurrentAccountId(), "手机之间测试都是好的", sipNumber);//unicode msg test
//                } catch (RemoteException e) {
//                    e.printStackTrace();
//                }
//            }
//        });
//
//        // Set on Incoming call listener
//        abtoPhone.setIncomingCallListener(new OnIncomingCallListener() {
//
//            public void OnIncomingCall(String contact, long accountId) {
//            	remoteContact = contact;
//            	//callId = abtoPhone.getActiveCallId();
//            	startAV(true);
//            }
//        }); //incoming call listener


        mVibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        initUI();
    }

    private void initUI() {
        initializeViews();
        addNumberFormatting();
        setClickListeners();
    }


    /**
     * Initializes the views from XML
     */
    private void initializeViews() {
        mPhoneNumberField = (EditText) findViewById(R.id.sip_number);
        mPhoneNumberField.setInputType(android.text.InputType.TYPE_NULL);

        mOneButton = (Button) findViewById(R.id.one);
        mTwoButton = (Button) findViewById(R.id.two);
        mThreeButton = (Button) findViewById(R.id.three);
        mFourButton = (Button) findViewById(R.id.four);
        mFiveButton = (Button) findViewById(R.id.five);
        mSixButton = (Button) findViewById(R.id.six);
        mSevenButton = (Button) findViewById(R.id.seven);
        mEightButton = (Button) findViewById(R.id.eight);
        mNineButton = (Button) findViewById(R.id.nine);
        mZeroButton = (Button) findViewById(R.id.zero);
        mStarButton = (Button) findViewById(R.id.asterisk);
        mPoundButton = (Button) findViewById(R.id.hash);
        mDeleteButton = (Button) findViewById(R.id.deleteButton);
    }

    /**
     * Adds number formatting to the field
     */
    private void addNumberFormatting() {
//        mPhoneNumberField.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
    }

    /**
     * Sets click listeners for the views
     */
    private void setClickListeners() {
        mZeroButton.setOnClickListener(this);
        mZeroButton.setOnLongClickListener(this);

        mOneButton.setOnClickListener(this);
        mTwoButton.setOnClickListener(this);
        mThreeButton.setOnClickListener(this);
        mFourButton.setOnClickListener(this);
        mFiveButton.setOnClickListener(this);
        mSixButton.setOnClickListener(this);
        mSevenButton.setOnClickListener(this);
        mEightButton.setOnClickListener(this);
        mNineButton.setOnClickListener(this);
        mStarButton.setOnClickListener(this);
        mPoundButton.setOnClickListener(this);

        mDeleteButton.setOnClickListener(this);
        mDeleteButton.setOnLongClickListener(this);
    }

    private void keyPressed(int keyCode) {
        mVibrator.vibrate(DURATION);
        KeyEvent event = new KeyEvent(KeyEvent.ACTION_DOWN, keyCode);
        mPhoneNumberField.onKeyDown(keyCode, event);
    }

    /**
     * Click handler for the views
     */
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.one: {
                keyPressed(KeyEvent.KEYCODE_1);
                return;
            }
            case R.id.two: {
                keyPressed(KeyEvent.KEYCODE_2);
                return;
            }
            case R.id.three: {
                keyPressed(KeyEvent.KEYCODE_3);
                return;
            }
            case R.id.four: {
                keyPressed(KeyEvent.KEYCODE_4);
                return;
            }
            case R.id.five: {
                keyPressed(KeyEvent.KEYCODE_5);
                return;
            }
            case R.id.six: {
                keyPressed(KeyEvent.KEYCODE_6);
                return;
            }
            case R.id.seven: {
                keyPressed(KeyEvent.KEYCODE_7);
                return;
            }
            case R.id.eight: {
                keyPressed(KeyEvent.KEYCODE_8);
                return;
            }
            case R.id.nine: {
                keyPressed(KeyEvent.KEYCODE_9);
                return;
            }
            case R.id.zero: {
                keyPressed(KeyEvent.KEYCODE_0);
                return;
            }
            case R.id.hash: {
                keyPressed(KeyEvent.KEYCODE_POUND);
                return;
            }
            case R.id.asterisk: {
                keyPressed(KeyEvent.KEYCODE_STAR);
                return;
            }
            case R.id.deleteButton: {
                keyPressed(KeyEvent.KEYCODE_DEL);
//                int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);
//
//                if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
//                    ActivityCompat.requestPermissions(
//                            this,
//                            new String[]{Manifest.permission.CALL_PHONE},
//                            Integer.parseInt("123"));
//                } else {
//                    startActivity(new Intent(Intent.ACTION_CALL).setData(Uri.parse("tel:12345678901")));
//                }
            }


        }

    }

    /**
     * Long Click Listener
     */
    @Override
    public boolean onLongClick(View view) {
        switch (view.getId()) {
            case R.id.deleteButton: {
                Editable digits = mPhoneNumberField.getText();
                digits.clear();
                return true;
            }
            case R.id.zero: {
                keyPressed(KeyEvent.KEYCODE_PLUS);
                return true;
            }
        }
        return false;
    }



    @Override
    public void onRegistrationFailed(long accId, int statusCode, String statusText) {

        if(dialog != null) dialog.dismiss();

        AlertDialog.Builder fail = new AlertDialog.Builder(MainActivity.this);
        fail.setTitle("Registration failed");
        fail.setMessage(statusCode + " - " + statusText);
        fail.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        fail.show();

        onUnRegistered(0);
    }

    @Override
    public void onRegistered(long accId) {
        //Toast.makeText(this, "MainActivity - onRegistered", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUnRegistered(long arg0) {

        if(dialog != null) dialog.dismiss();

        //Unsubscribe reg events
        abtoPhone.setRegistrationStateListener(null);


        //Start reg activity
        startActivity(new Intent(MainActivity.this, RegisterActivity.class));

        //Close this activity
        finish();
    }


    public void startCall(boolean bVideo)   {
        //Get phone number to dial
        String sipNumber = callUri.getText().toString();
        if(TextUtils.isEmpty(sipNumber))  return;

        if(TextUtils.isEmpty(domain) && !sipNumber.contains("@") ) {
            Toast.makeText(this, "Specify remote side address as 'number@domain:port'", Toast.LENGTH_SHORT).show();
            return;
        }

        // Start new call
        try {
            if(bVideo) abtoPhone.startVideoCall(sipNumber, abtoPhone.getCurrentAccountId());
            else       abtoPhone.startCall(sipNumber, abtoPhone.getCurrentAccountId());

            if(!sipNumber.contains("sip:") ) sipNumber = "sip:" + sipNumber;
            if(!sipNumber.contains("@") ){
                remoteContact = String.format("<%1$s@%2$s>", sipNumber, domain);
            }else{
                remoteContact = String.format("<%1$s>", sipNumber);
            }

            startAV(false);

        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {

    	super.onResume();
    }


    public void onDestroy() {
        super.onDestroy();
        abtoPhone.setRegistrationStateListener(null);
    }


    public void onStop() {
        super.onStop();
    }


    @Override
    public void onBackPressed() {

        if(accExpire==0) {
            onUnRegistered(0);
            return;
        }
        try {
//            if(dialog==null)
//            {
//                dialog = new ProgressDialog(this);
//                dialog.setCancelable(false);
//                dialog.setMessage("Unregistering...");
//            }
//            dialog.show();

            abtoPhone.unregister();

            finish();
            System.exit(0);

        } catch (RemoteException e) {
            e.printStackTrace();
            dialog.dismiss();
        }
    }


    
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.activity_main, menu);
//        return true;
//    }

    private synchronized void startAV(boolean incoming) {
        Intent intent = new Intent(this, ScreenAV.class);
        intent.putExtra("incoming", incoming);
        intent.putExtra(ScreenAV.CALL_ID, abtoPhone.getActiveCallId());
        intent.putExtra(AbtoPhone.REMOTE_CONTACT, remoteContact);
        //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }


}
