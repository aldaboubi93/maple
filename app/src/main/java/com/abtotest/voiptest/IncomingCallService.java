package com.abtotest.voiptest;

import org.abtollc.sdk.AbtoApplication;
import org.abtollc.sdk.AbtoPhone;
import org.abtollc.sdk.OnIncomingCallListener;
import org.abtollc.sdk.OnInitializeListener;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.util.Log;

public class IncomingCallService extends Service implements OnIncomingCallListener, OnInitializeListener {

	private AbtoPhone abtoPhone;

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		abtoPhone = ((AbtoApplication) getApplication()).getAbtoPhone();

		abtoPhone.setIncomingCallListener(this);
		abtoPhone.setInitializeListener(this);

		return START_STICKY;
	}

	@Override
	public void onInitializeState(OnInitializeListener.InitializeState state, String message) {
		if (state != InitializeState.SUCCESS) return;

		long accId = abtoPhone.getConfig().addAccount(RegisterActivity.RegDomain, null, RegisterActivity.RegUser, RegisterActivity.RegPassword, null, "", 300, false);

		//Register
		try {
			abtoPhone.register();
		} catch (RemoteException ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void OnIncomingCall(String remoteContact, long arg1) {

		Intent intent = new Intent(this, ScreenAV.class);
		intent.putExtra("incoming", true);
		intent.putExtra(ScreenAV.CALL_ID, abtoPhone.getActiveCallId());
		intent.putExtra(AbtoPhone.REMOTE_CONTACT, remoteContact);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);



	}
}
